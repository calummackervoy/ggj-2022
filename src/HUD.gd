extends Control
class_name HUD

onready var game := get_tree().current_scene
onready var turn_label := game.get_node("Sprite3D/Viewport/RichTextLabel")

var colors : PoolColorArray =["e8191700", "e8330a00", "e8080706"]
func _ready() -> void:
	Signals.connect("state_changed", self, "on_state_changed")

func on_state_changed(new_state):
#	print("HUD.on_state_changed %s" % [new_state])
	
	var state_text = ""
	turn_label.set("custom_colors/font_color", colors[-1])
	match new_state:
		StateManager.STATES.NOT_READY:
			state_text = "Loading..."
		StateManager.STATES.PLAYER_1_TURN:
			state_text = "PLAYER 1"
			turn_label.set("custom_colors/font_color", colors[Globals.PLAYER.ONE])
		StateManager.STATES.PLAYER_2_TURN:
			state_text = "PLAYER 2"
			turn_label.set("custom_colors/font_color", colors[Globals.PLAYER.TWO])
		StateManager.STATES.WAIT:
			state_text = "wait"
		StateManager.STATES.EXECUTE_ACTIONS:
			state_text = "Moving"
		StateManager.STATES.WIN:
			state_text = "%s WINS!!" % [game.won_player.name]
			var color_idx = 0
			if !game.won_player.is_player1 : color_idx = 1
			turn_label.set("custom_colors/font_color", colors[color_idx])
			self.show()
			get_node("Control/RichTextLabel").bbcode_text = "[wave amp=50 freq=3] %s WINS !![/wave]" % game.won_player.name
		_:
			state_text = "??? %s" % [new_state]
			
	turn_label.text = "%s" % [state_text]
