extends Spatial
class_name Game

export(bool) var DEBUG = true

onready var game := get_tree().current_scene
onready var grid := game.get_node("GridMap")
onready var player1 := grid.get_node("Player1")
onready var player2 := grid.get_node("Player2")
onready var state_manager = game.get_node("StateManager")
onready var hud := game.get_node("HUD")
onready var piece_prompt = game.get_node("CanvasLayer").get_node("PiecePrompt")

var pawn_scene = preload("res://src/Pawn.tscn")

# collision shapes used for detecting selection
# the tile and piece scenes select themselves (populate these variables)
var hovered_tile : TileArea
var hovered_piece: Spatial

var selected_piece_map_position: Vector3
var selected_piece: Spatial
var won_player: Player

func _ready() -> void:
	
#	1. Setup game
	player1.is_player1 = true
	player2.is_player1 = false

	Signals.connect("state_changed", self, "on_state_changed")
	
	init_game()

func reset_game():
	grid.clear_everything()
	player1.clear_everything()
	player2.clear_everything()
	state_manager.reset_game()
	get_tree().reload_current_scene()

func init_game():
	# sets up pieces and starts player 1 turn
	_add_to_piece_tray(0, 6)
	_add_to_piece_tray(1, 6)
	
	_add_kings()
	
	state_manager.start_player_turn(player1)

func on_state_changed(new_state):
	
	match new_state:
		StateManager.STATES.PLAYER_1_TURN, StateManager.STATES.PLAYER_2_TURN:
			var player1_turn = state_manager.current_player == player1
			player1.highlight_tray(player1_turn)
			player2.highlight_tray(!player1_turn)
		StateManager.STATES.EXECUTE_ACTIONS:
			execute_player_actions()
			
func _input(event):
	if state_manager.current_state == state_manager.STATES.PLAYER_1_TURN or state_manager.current_state == state_manager.STATES.PLAYER_2_TURN:
		handle_player_turn(event)
	# press any key to reset the game after someone wins
	elif state_manager.current_state == state_manager.STATES.WIN and (event is InputEventKey and event.pressed):
		reset_game()
		return

func _instantiate_piece() -> Spatial:
	# NOTE: in Godot 4.0 instance() will become instantiate()
	return pawn_scene.instance()

func _add_kings():
	for p in range(2):
		var pawn := _instantiate_piece()
		var map_pos : Vector3 = Vector3(0, 0, floor(grid.grid_height * 0.5))
		if p == Globals.PLAYER.TWO : 
			map_pos.x = grid.grid_width - 1
			
		pawn.player_number = p
		pawn.piece_type =  Globals.PIECE_TYPE.KING
		
		_add_to_board(pawn, p)
		grid.place_in_cell(pawn, map_pos, true)
		
func _add_to_board(pawn, player_index: int = 0):
	match player_index :
		Globals.PLAYER.ONE :
			player1.get_node("Pawns").add_child(pawn)
		Globals.PLAYER.TWO:
			player2.get_node("Pawns").add_child(pawn)
			
func _add_to_piece_tray(player_index: int = 0, count: int = 1):
	if player_index == -1:
		return
	
	for i in range(count):
		var pawn := _instantiate_piece()
		pawn.player_number = player_index
		# generate one of each piece type ending with pawns, no kings (which are 0)
		pawn.piece_type = clamp(i + 1, 0, Globals.PIECE_TYPE.size() - 1)
		
		if(grid.append_to_piece_tray(pawn, player_index)) == -1:
			pawn.queue_free()
			return
		
		match player_index:
			0:
				player1.add_to_piece_tray(pawn)
			1:
				player2.add_to_piece_tray(pawn)

func _clear_selected_piece():
	if selected_piece != null:
		selected_piece.show()
		selected_piece = null
	if piece_prompt != null:
		piece_prompt.clear()

# sets the x value to that of the first row
func _clamp_map_coords_to_player_row(map_cell_coords: Vector3) -> Vector3:
	if !DEBUG:
		match state_manager.get_current_player_index():
			Globals.PLAYER.ONE:
				map_cell_coords.x = 0
			Globals.PLAYER.TWO:
				map_cell_coords.x = grid.grid_width - 1
	
	return map_cell_coords

func handle_player_turn(event):
	
	if Input.is_action_pressed("pick_pawn"):
		
		# select a piece for movement
		if selected_piece == null:
			var player_index = state_manager.get_current_player_index()
			if hovered_piece != null and hovered_piece.player_number == player_index and hovered_piece.get_parent().name == "PieceTray":
				_clear_selected_piece()
				selected_piece = hovered_piece
				selected_piece_map_position = grid.world_to_piece_tray_map(hovered_piece.translation, player_index == 0)
				selected_piece.hide()
				
				# This stuff is for clamping the position of the piece prompt to the first row
				# to indicate to the player that they can't place it anywhere they like
				# the hacky part is that it's coupled with the size of the grid
				# ran out of time to change this
				var first = grid.get_used_cells()[10]
				var clamp_x = grid.map_to_world(0, 0, 3).x + selected_piece.translation.x
				
				# HACKY MAGIC NUMBERS SORRY! i tried doing it better but I couldn't get the
				# world_pos from the first column of the grid :(
				if player_index == Globals.PLAYER.ONE:
					clamp_x = 185
				else:
					clamp_x = 845
				
				piece_prompt.init_piece(player_index, hovered_piece.piece_type, clamp_x)
		
		elif selected_piece != null:
			
			# hovered tile has been populated with the tile which is being hovered over
			if hovered_tile != null and selected_piece != null:
				
				var hovered_tile_cell_pos = _clamp_map_coords_to_player_row(
					grid.world_to_map(hovered_tile.translation))
				
				# check first if the cell choice is empty
				if !grid.can_move_to_cell(hovered_tile_cell_pos):
					return
				
				grid.move_from_piece_tray_to_board(state_manager.get_current_player_index(), selected_piece_map_position, hovered_tile_cell_pos)
				
				state_manager.current_player.move_from_tray_to_board(selected_piece)
				_clear_selected_piece()
				state_manager.finish_player_turn()
	
	elif Input.is_action_pressed("ui_cancel"):
		_clear_selected_piece()

func execute_player_actions():
	yield(player1.execute_turn_actions(), "completed")
	yield(get_tree().create_timer(0.2), "timeout")
	yield(player2.execute_turn_actions(), "completed")
	yield(get_tree().create_timer(0.2), "timeout")
	
	state_manager.finish_execute_player_actions()

func won_player(player):
	won_player = player
