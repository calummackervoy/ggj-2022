extends Spatial
class_name Player

onready var game := get_tree().current_scene
onready var player := self
onready var piece_tray := player.get_node("PieceTray")
onready var pawns := player.get_node("Pawns")
#onready var king := pawns.get_node("King")

var is_player1 : bool

var actions := []

func highlight_tray(highlight: bool):
	for pawn in piece_tray.get_children():
#		print("highlight pawn %s? highlight: %s" % [pawn, highlight])
		pawn.highlight(highlight)

func add_pawn(pawn: Spatial):
	pawns.add_child(pawn)

func remove_pawn(pawn: Spatial):
	pawns.remove_child(pawn)

func add_to_piece_tray(piece: Spatial):
	piece_tray.add_child(piece)

func remove_from_piece_tray(piece: Spatial):
	piece_tray.remove_child(piece)

func move_from_tray_to_board(piece: Spatial):
	piece_tray.remove_child(piece)
	pawns.add_child(piece)

func move_from_board_to_tray(piece: Spatial):
	pawns.remove_child(piece)
	piece_tray.add_child(piece)

func clear_pawns():
	for child in pawns.get_children():
		pawns.remove_child(child)
		child.queue_free()

func clear_piece_tray():
	for child in piece_tray.get_children():
		piece_tray.remove_child(child)
		child.queue_free()

func clear_everything():
	clear_pawns()
	clear_piece_tray()

func execute_turn_actions():
	if pawns.get_child_count() == 0:
		yield(get_tree().create_timer(0), "timeout")
	
	for pawn in pawns.get_children():
		# end turn execution early if someone won
		if game.won_player != null:
			yield(get_tree().create_timer(0), "timeout")
			return
		
		yield(pawn.move(is_player1), "completed")
	
	for pawn in pawns.get_children():
		print('checking pawn position ' + str(pawn))
		pawn.check_rebound()
