extends Spatial
class_name Pawn

onready var game := get_tree().current_scene
onready var grid = game.get_node("GridMap")
onready var sprites := get_node("Sprites")

var current_destination_cell: Vector3

export(Globals.PIECE_TYPE) var piece_type = Globals.PIECE_TYPE.PAWN
export(Globals.PLAYER) var player_number = Globals.PLAYER.ONE

func _ready():
	set_piece_type(piece_type)
	set_player(player_number)
	name = to_string()

func _to_string():
	var piece_name
	match piece_type:
		Globals.PIECE_TYPE.BISHOP:
			piece_name = "BISHOP"
		Globals.PIECE_TYPE.KING:
			piece_name = "KING"
		Globals.PIECE_TYPE.KNIGHT:
			piece_name = "KNIGHT"
		Globals.PIECE_TYPE.QUEEN:
			piece_name = "QUEEN"
		Globals.PIECE_TYPE.ROOK:
			piece_name = "ROOK"
		Globals.PIECE_TYPE.PAWN:
			piece_name = "PAWN"
		_:
			piece_name = "??_%s_??" % [piece_type]
			
	match player_number:
		Globals.PLAYER.ONE:
			piece_name += "_P1"
		Globals.PLAYER.TWO:
			piece_name += "_P2"  
			
	return piece_name

func _get_player():
	match player_number:
		Globals.PLAYER.ONE:
			return game.player1
		Globals.PLAYER.TWO:
			return game.player2
	return null

func _get_enemy_position_x() -> int:
	# returns a Vector3 with the coordinates of the enemy camp
	return 0 if player_number == 1 else game.grid.grid_width - 1

func highlight(highlight: bool):
#	var alpha = 1
#	if not highlight:
#		alpha = 0.5
#	sprites.get_node("Back").modulate.a = alpha
	var h_sprite : Sprite3D = sprites.get_node("Highlight")
	
	if highlight:
		h_sprite.hide()
	else:
		h_sprite.show()
func move(movement_left: bool):
	
	yield(move_forward(movement_left), "completed")

func move_random():

	if not check_take_piece():

		var valid_move = false
		var world_movement
		var loop_idx = 0
		while not valid_move:

			var random_idx = randi() % 2
			var offset: Vector3
			match random_idx:
				0:
					offset = Vector3(+1, 0, 0)
				1:
					offset = Vector3(-1, 0, 0)
#				2:
#					offset = Vector3(0, 0, +1)
#				3:
#					offset = Vector3(0, 0, -1)
					
					
					
			var request_response = game.grid.request_move_with_offset(self, offset)
			valid_move = request_response[0]
			world_movement = request_response[1]
			
			loop_idx += 1
			
			if loop_idx > 10:
				print("YOU UNLOCKED AN INFINITE LOOP!!")
#				yield()
				break
				
		if valid_move:
			yield(move_animation(world_movement + translation), "completed")
		else:
			yield()
		
	else:
		yield(get_tree().create_timer(0), "timeout")

func move_forward(movement_left: bool):
	if yield(check_take_piece(), "completed"):
		return
		
	else:
		yield(check_move_piece(), "completed")

func check_rebound(pawn_cell_pos: Vector3 = game.grid.world_to_map_rounded_x(translation)):
	# if I am in the enemy last row, and there is no piece I can take,
	# then I will be rebounded to piece tray
	if pawn_cell_pos.x == _get_enemy_position_x():
		
		if !can_take_piece():
			print('rebounding pawn in position ' + str(pawn_cell_pos))
			_get_player().move_from_board_to_tray(self)
			game.grid.move_from_board_to_piece_tray(player_number, pawn_cell_pos)

func move_animation(destination: Vector3):
	# move to the destination cell!
	var destination_cell = game.grid.world_to_map_rounded_x(destination)
	game.grid.move_to_cell(self, destination_cell)
	
	# animate!
	$Tween.interpolate_property(self, "translation", self.translation, destination, .5,Tween.TRANS_EXPO,Tween.EASE_IN_OUT )
	$Tween.start()
	yield($Tween, "tween_completed")

func can_take_piece() -> bool:
	# will be set to false when a piece has just been taken
	return game.grid.get_takeable_list(self).size() > 0

func check_take_piece() -> bool:
	# will be set to false when a piece has just been taken
	var list_of_takeable : Array = game.grid.get_takeable_list(self)
	var my_map_trans : Vector3 = game.grid.world_to_map_rounded_x(translation)
	
	if list_of_takeable.size() != 0:
		var random_idx = randi() % list_of_takeable.size()
		yield(take_piece(list_of_takeable[random_idx]), "completed")
		return true
	
	yield(get_tree().create_timer(0), "timeout")
	return false
	
func check_move_piece() : 
	var destinations : PoolVector3Array = game.grid.get_moveable_list(self)
			
	if destinations.size() == 0 : 
		yield(get_tree().create_timer(0),"timeout")
		return
		
	var final_map_destination : Vector3 = destinations[randi()%destinations.size()]
	var final_world_destination : Vector3 = game.grid.map_to_world(final_map_destination.x, final_map_destination.y, final_map_destination.z)
	yield(move_animation(final_world_destination), "completed")

func take_piece(to_be_taken_piece):
	var destination = to_be_taken_piece.translation
	var map_destination = game.grid.world_to_map_rounded_x(destination)
	var start_position = game.grid.world_to_map_rounded_x(translation)
	$Tween.interpolate_property(self, "translation", self.translation, destination, .5,Tween.TRANS_EXPO,Tween.EASE_IN_OUT )
	$Tween.start()
	
	# update taken piece alegiance
	to_be_taken_piece.set_player(player_number)
	# this will set the board position I'm moving to as null
	if game.grid.move_from_board_to_piece_tray(player_number, map_destination) == -1:
		to_be_taken_piece.queue_free()
	# this will update the player scene with the piece membership
	match player_number:
		Globals.PLAYER.ONE:
			game.player2.remove_pawn(to_be_taken_piece)
			game.player1.add_to_piece_tray(to_be_taken_piece)
		Globals.PLAYER.TWO:
			game.player1.remove_pawn(to_be_taken_piece)
			game.player2.add_to_piece_tray(to_be_taken_piece)
	
	# this will empty the cell where I was
	game.grid.empty_cell(start_position)
	game.grid.place_in_cell(self, map_destination, false)
	
	# if I took the king, I win
	if to_be_taken_piece.piece_type == Globals.PIECE_TYPE.KING:
		match player_number:
			Globals.PLAYER.ONE:
				game.state_manager.player_won(game.player1)
			Globals.PLAYER.TWO:
				game.state_manager.player_won(game.player2)
	
	yield(get_tree().create_timer(0.6), "timeout")

func set_player(player):
	player_number = player
	sprites.get_node("Front").modulate = Globals.player_colors[player]
	
func set_piece_type(p_type):
	piece_type = p_type
	for sprite in sprites.get_children():
		sprite.frame_coords.x = p_type

func _on_SelectArea_mouse_entered():
	game.hovered_piece = self
