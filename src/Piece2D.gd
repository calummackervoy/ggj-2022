extends Node2D

onready var game = get_tree().current_scene
onready var canvas_layer : CanvasLayer = game.get_node("CanvasLayer")

export(Globals.PIECE_TYPE) var piece_type = Globals.PIECE_TYPE.PAWN
export(Globals.PLAYER) var player_number = Globals.PLAYER.ONE

var clamp_x : float

func _ready():
	init_piece(player_number, piece_type, 0)
	clear()

func _process(delta):
	var next_position = get_global_mouse_position()
	
	if not game.DEBUG:
		next_position.x = clamp(next_position.x, clamp_x, clamp_x)
	
	set_position(next_position)
	
func init_piece(player_num, piece_t, clamp_x: float):
	set_player(player_num)
	set_piece_type(piece_t)
	self.show()
	self.clamp_x = clamp_x
	set_process(true)

func clear():
	self.hide()
	set_process(false)
	
func set_player(player):
	player_number = player
	get_node("Front").modulate = Globals.player_colors[player_number]
		
func set_piece_type(p_type):
	for sprite in get_children():
		sprite.frame_coords.x = p_type
