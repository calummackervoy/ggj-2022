#!/bin/bash

rm release.zip
rm -rf zip/src
rm -rf zip/release
mkdir zip

rsync -av \
    --exclude='publish' \
    --exclude='.import' \
    --exclude='.git' \
    --exclude='Builds' \
    ../. \
    zip/src

rsync -av \
    ../Builds/ \
    zip/release/

cd zip
zip -r ../release.zip *