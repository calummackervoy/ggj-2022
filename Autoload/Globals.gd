extends Node

enum CELL_TYPE { 
	EMPTY   = -1,
	PAWN  	= 0, 
}

enum PIECE_TYPE {
	KING = 0, # we rely on king being 0 in startup so that we don't generate one in the piece tray
	QUEEN = 1, 
	ROOK = 2, 
	BISHOP = 3, 
	KNIGHT = 4, 
	PAWN = 5, # we rely on pawn being last in startup so that the last generated pieces are all pawns
}

enum PLAYER {
	ONE = 0, 
	TWO = 1
}

var player_colors : PoolColorArray = [
	Color("afa870"),
	Color("a86e5f"),
]
	

