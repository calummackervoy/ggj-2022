extends Node
class_name StateManager

enum STATES { 
	NOT_READY			= 0,
	MAIN_MENU 			= 1,
	PAUSE				= 2,
	WAIT				= 3,
	PLAYER_1_TURN		= 4,
	PLAYER_2_TURN		= 5,
	EXECUTE_ACTIONS		= 6,
	WIN					= 7,
}

onready var game := get_tree().current_scene

var current_player : Player
var current_state = STATES.NOT_READY
var previous_state = STATES.NOT_READY

func get_current_player_index():
	#
	#	:return: 0 if currently player 1, 1 if currently player 2. -1 if neither
	#
	match current_state:
		STATES.PLAYER_1_TURN:
			return Globals.PLAYER.ONE
		STATES.PLAYER_2_TURN:
			return Globals.PLAYER.TWO
	return -1

func finish_player_turn():
	if current_state == STATES.PLAYER_1_TURN:
		start_next_player_turn()
	else:
		change_state(STATES.EXECUTE_ACTIONS)

func finish_execute_player_actions():
	change_state(STATES.PLAYER_1_TURN)
	
func start_next_player_turn():
#	print("start_next_player_turn")
	
	var next_player : Player
	if current_state == STATES.PLAYER_1_TURN:
		next_player = game.player2
	else:
		print("wrong state!")
		
	start_player_turn(next_player)
	
func start_player_turn(player: Player):
#	print("start_player_turn %s" % [player])
	
	if player == game.player1:
		change_state(STATES.PLAYER_1_TURN)
	else:
		change_state(STATES.PLAYER_2_TURN)
	
	# handle the situation where the player has no possible moves
	if current_player.piece_tray.get_child_count() == 0:
		# if I also have no board pieces, I lose
		if current_player.pawns.get_child_count() == 0:
			current_player = game.player1
			player_won()
		
		else:
			finish_player_turn()

func player_won(winning_player=current_player):
	game.won_player(winning_player)
	change_state(STATES.WIN)

func start_wait():
	change_state(STATES.WAIT)
	
func resume_wait():
	change_state(previous_state)
	
func change_state(new_state):
	# can't use change_state to change from win status (must reset game)
	if current_state == STATES.WIN:
		print('rejected change from win state. Use reset_game if you want to do this')
		return
	
#	print("change_state %s" % [new_state])
	previous_state = current_state
	current_state = new_state
	
	if current_state == STATES.PLAYER_1_TURN:
		current_player = game.player1
	elif current_state == STATES.PLAYER_2_TURN:
		current_player = game.player2
	
	Signals.emit_signal("state_changed", new_state)

func reset_game():
	current_state = 0
	get_tree().create_timer(2)
