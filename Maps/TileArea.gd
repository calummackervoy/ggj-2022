extends Area
class_name TileArea

onready var game = get_tree().current_scene 

func _ready() -> void:
	pass # Replace with function body.

func _on_Area_mouse_entered() -> void:
	game.hovered_tile = self

func _on_Area_mouse_exited() -> void:
	if game.hovered_tile == self:
		game.hovered_tile = null
