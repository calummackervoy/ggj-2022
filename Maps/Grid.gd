extends GridMap

export var grid_width: int
export var grid_height: int
export var piece_tray_width: int

onready var game := get_tree().current_scene
onready var tile_step_translation := cell_size.x
onready var piece_tray_height := grid_height
onready var margin_map_size = Vector3(1, 0, 0)
# an array with references to objects inhabiting a tile
var inhabitants = []
# an array with references to the piece trays of each player
var player_piece_trays = []

var half_cell_size = cell_size * 0.5
var cell_length = cell_size.x

var tile_area_scene = preload("res://Maps/TileArea.tscn")

const TILE_STEP = Vector3(1,0,0)

func _ready():
	# initialise the inhabitants map
	
	for cell in get_used_cells():
		var tile_area : TileArea = tile_area_scene.instance()
		tile_area.translation = map_to_world(cell.x, cell.y, cell.z)
		add_child(tile_area)
	
	for x in range(grid_width):
		inhabitants.append([])
		inhabitants[x].resize(grid_height)
	
	# add a piece tray for each player (where pieces are deployed from)
	player_piece_trays.append([])
	player_piece_trays.append([])
	
	for x in range(piece_tray_width):
		player_piece_trays[0].append([])
		player_piece_trays[1].append([])
		player_piece_trays[0][x].resize(piece_tray_height)
		player_piece_trays[1][x].resize(piece_tray_height)

# movement_left: use a negative step to move right
# :return: [valid_request: Bool, world_movement: Vector3]

func request_move(actor, map_destination):
	
	var map_movement: Vector3 = TILE_STEP if actor.player_number == 1 else -TILE_STEP
#	var world_movement = map_movement.normalized() * tile_step_translation

	var cell_start : Vector3 = world_to_map_rounded_x(actor.translation)
	var cell_target : Vector3 = map_destination

	if !can_move_to_cell(cell_target):
		return false
	
	print("moving " + str(cell_start) + " to " + str(cell_target))
	move_to_cell(actor, cell_target)

	return true

func request_move_direction(actor, movement_left: bool):

	var map_movement: Vector3 = TILE_STEP if movement_left else -TILE_STEP
	return request_move_with_offset(actor, map_movement)

func request_move_with_offset(actor, movement_offset: Vector3):
	
	var world_movement = movement_offset.normalized() * tile_step_translation

	var cell_start = world_to_map_rounded_x(actor.translation)
	var cell_target = cell_start + movement_offset
	
	if !can_move_to_cell(cell_target):
#		print("not moving! %s" % [actor.piece_type])
		return [false, Vector3(-99, -99, -99)]
	
#	print("moving type %s, start %s to %s" % [actor.piece_type, cell_start, cell_target])
	move_to_cell(actor, cell_target)

	return [true, world_movement]

func move_to_cell(node, cell_target: Vector3):
	if !can_move_to_cell(cell_target):
		print('tried to move to cell ' + str(cell_target) + ' but failed')
		return
	
	var node_cell = world_to_map_rounded_x(node.translation)
	
	empty_cell(node_cell)
	place_in_cell(node, cell_target, false)
	
	return map_to_world(cell_target.x, cell_target.y, cell_target.z)

func empty_cell(cell: Vector3):
	assert(cell_within_bounds(cell), "Cell index out of bounds!")
	
	inhabitants[cell.x][cell.z] = null

func place_in_cell(node, cell_position: Vector3, set_physical_position: bool = false):
	#
	#	will attempt to place the node in the cell
	#	if it's occupied, it will return -1
	#	
	if !can_move_to_cell(cell_position):
		print('attempted to place in cell %s but failed because can_move_to_cell rejected it', [str(cell_position)])
		return -1
	
	# store references to the node's position
	inhabitants[cell_position.x][cell_position.z] = node
	
	# set the node's physical position on screen
	if set_physical_position:
		node.translation = map_to_world(cell_position.x, cell_position.y, cell_position.z)

func get_node_in_cell(cell: Vector3):
	assert(cell_within_bounds(cell), "Cell index out of bounds!")
	
	var grid_pos = Vector3(cell.x / cell_length, cell.y / cell_length, cell.z / cell_length)
	return inhabitants[cell.x][cell.z]

func can_move_to_coords(target_coords: Vector3):
	return can_move_to_cell(world_to_map_rounded_x(target_coords))

func cell_within_bounds(cell_position: Vector3):
	return cell_position.x >= 0 and cell_position.x < grid_width and cell_position.z >= 0 and cell_position.z < grid_height

func can_move_to_cell(target_cell: Vector3):
	return cell_within_bounds(target_cell) and inhabitants[target_cell.x][target_cell.z] == null

func append_to_piece_tray(piece: Node, player_index: int):
	#
	#	will attempt to place the node in the piece tray
	#	if it's full, it will return -1
	#
	for x in range(player_piece_trays[player_index].size()):
		
		for z in range(player_piece_trays[player_index][x].size()):
			
			if player_piece_trays[player_index][x][z] == null:
				# place in map
				player_piece_trays[player_index][x][z] = piece
				var map_position = Vector3(x, 0, z)
				
				# place in world
				piece.set_translation(piece_tray_map_to_world(map_position, player_index == 0))
				return
	
	print('ERR! No empty piece tray found! We dont have code to handle this yet..')
	return -1

func move_from_piece_tray_to_board(player_index: int, map_position: Vector3, board_position: Vector3):
	#
	#  :param player_index: 0 for player 1, 1 for player 2
	#  :param map_position: a Vector3 with the local piece tray map coords (use world_to_piece_tray_map)
	#  :param board_position: a Vector3 with the board co-ordinates (board grid map)
	#  :return: -1 if no piece is in the map_position, or if the board position is occupied
	#  removes the piece from the piece tray, and moves them onto the board position
	#
	if !can_move_to_cell(board_position):
		return -1
	
	var piece = player_piece_trays[player_index][map_position.x][map_position.z]
	
	if piece == null:
		return -1
	
	player_piece_trays[player_index][map_position.x][map_position.z] = null
	place_in_cell(piece, board_position, true)

func move_from_board_to_piece_tray(player_index: int, board_position: Vector3):
	#
	#  :param player_index: 0 for player 1, 1 for player 2
	#  :param board_position: a Vector3 with the board co-ordinates (board grid map)
	#  :return: -1 if the piece tray is full or there was no piece in the board_position
	#  removes the piece from the piece tray, and moves them onto the board position
	#
	var piece = inhabitants[board_position.x][board_position.z]
	
	if piece == null:
		print('move_from_board_to_piece_tray failed because no piece in board_position ' + str(board_position))
		return -1
	
	if append_to_piece_tray(piece, player_index) == -1:
		print('move_from_board_to_piece_tray failed in append_to_piece_tray')
		return -1
	
	inhabitants[board_position.x][board_position.z] = null

func world_to_map_rounded_x(trans: Vector3):
	trans.x = round(trans.x)
	return world_to_map(trans)

func piece_tray_map_to_world(target_cell: Vector3, is_left_hand_side: bool = true):
	# transform from local piece tray map to global grid map
	
	if is_left_hand_side:
		target_cell = target_cell - Vector3(piece_tray_width, 0, 0) - margin_map_size
	
	else:
		target_cell = target_cell + Vector3(grid_width, 0, 0) + margin_map_size
	
	return map_to_world(target_cell.x, target_cell.y, target_cell.z)

func world_to_piece_tray_map(trans: Vector3, is_left_hand_side: bool = true) -> Vector2:
	
	var global_map_pos = world_to_map_rounded_x(trans)
	
	if is_left_hand_side:
		return global_map_pos + Vector3(piece_tray_width, 0, 0) + margin_map_size
	
	else:
		return global_map_pos - Vector3(grid_width, 0, 0) - margin_map_size
		
func get_moveable_list(piece, type_overide = null):
	var piece_map_trans : Vector3 = world_to_map_rounded_x(piece.translation)
	var direction : int = 1
	if piece.player_number == Globals.PLAYER.TWO : direction = -1
	var destinations : PoolVector3Array = []
	var p_type = piece.piece_type
	if type_overide != null : p_type = type_overide
	match p_type :
		 
		Globals.PIECE_TYPE.KING:
			var map_trans : Vector3
			for i in range(2):
				for j in range(2):
#					((i+1)*2-3
					map_trans = piece_map_trans + Vector3((i+1)*2-3, 0, (j+1)*2-3)
					var is_request_valid = can_move_to_cell(map_trans)
					if is_request_valid :
						destinations.append(map_trans)
						
		Globals.PIECE_TYPE.QUEEN:
			destinations += get_moveable_list(piece, Globals.PIECE_TYPE.ROOK)
			destinations += get_moveable_list(piece, Globals.PIECE_TYPE.BISHOP)
			
		Globals.PIECE_TYPE.ROOK:
			var unique_destination
			var map_trans : Vector3
			for j in range(5):
				map_trans = piece_map_trans + Vector3(direction * (j+1), 0, 0)
				var is_request_valid = can_move_to_cell(map_trans)
				if is_request_valid :
					unique_destination = map_trans
				else :
					break
			if unique_destination != null:
				destinations.append(unique_destination)
				
		Globals.PIECE_TYPE.BISHOP:
			for i in range(2):
				var unique_destination = null
				var map_trans : Vector3
				for j in range(6):
					map_trans = piece_map_trans + Vector3(direction * (j+1), 0, ((i+1)*2-3) * (j+1))
					var is_request_valid = can_move_to_cell(map_trans)
					if is_request_valid :
						unique_destination = map_trans
					else :
						break
				if unique_destination != null:
					destinations.append(unique_destination)
					
		Globals.PIECE_TYPE.KNIGHT:
			#  z in [-1;1] 
			for i in range(2):
				# x = 2
				var new_map_trans : Vector3 = piece_map_trans + Vector3(direction * 2, 0, (i+1)*2-3 )
				var is_request_valid = can_move_to_cell(new_map_trans)
				if is_request_valid :
					destinations.append(new_map_trans)
					break
					
		Globals.PIECE_TYPE.PAWN:
			var new_map_trans : Vector3 = piece_map_trans + Vector3(direction , 0, 0)
			if not cell_within_bounds(new_map_trans) : return
			var is_request_valid = can_move_to_cell(new_map_trans)
#			print("valid? ", valid_request)
			if is_request_valid :
				destinations.append(new_map_trans)
	return destinations

func get_takeable_list(piece, type_overide = null) -> Array:
	var direction : int = 1
	if piece.player_number == Globals.PLAYER.TWO :
		direction = -1
	var list_of_takable_pieces : Array = []
	var piece_map_trans : Vector3 = world_to_map_rounded_x(piece.translation)
	var new_map_trans : Vector3
	var piece_type = piece.piece_type
	if type_overide != null : piece_type = type_overide

	match piece_type :
		Globals.PIECE_TYPE.KING:
			for i in range(2):
				for j in range(2):
#					((i+1)*2-3
					new_map_trans = piece_map_trans + Vector3((i+1)*2-3, 0, (j+1)*2-3)
					if not cell_within_bounds(new_map_trans) : continue
					var takeable_piece : Spatial
					takeable_piece = get_node_in_cell(new_map_trans)
					if takeable_piece == null:
						continue
					if takeable_piece.player_number != piece.player_number:
						list_of_takable_pieces.append(takeable_piece)
						
		Globals.PIECE_TYPE.QUEEN:
			list_of_takable_pieces += get_takeable_list(piece, Globals.PIECE_TYPE.ROOK)
			list_of_takable_pieces += get_takeable_list(piece, Globals.PIECE_TYPE.BISHOP)
			
		Globals.PIECE_TYPE.ROOK:
			var n = 1
			for i in range(3):
				if i == 2 : n = 2
				for j in range(5):
					new_map_trans = piece_map_trans + Vector3(direction * (j+1) * (n - 1) , 0, ((i+1)*2-3) * (j+1) * (2 - n))
					if not cell_within_bounds(new_map_trans) : break
					var takeable_piece : Spatial
					takeable_piece=get_node_in_cell(new_map_trans)
					if takeable_piece == null:
						continue
					if takeable_piece.player_number != piece.player_number:
						list_of_takable_pieces.append(takeable_piece)
						
		Globals.PIECE_TYPE.BISHOP:
			for i in range(2):
				for j in range(6):
					new_map_trans = piece_map_trans + Vector3(direction * (j+1), 0, ((i+1)*2-3) * (j+1))
					if not cell_within_bounds(new_map_trans) : break
					var takeable_piece : Spatial
					takeable_piece=get_node_in_cell(new_map_trans)
					if takeable_piece == null:
						continue
					if takeable_piece.player_number != piece.player_number:
						list_of_takable_pieces.append(takeable_piece)
						
		Globals.PIECE_TYPE.KNIGHT:
			#  z in [-1;1] 
			for i in range(2):
				# x = 2
				new_map_trans = piece_map_trans + Vector3(direction * 2, 0, (i+1)*2-3 )
				if not cell_within_bounds(new_map_trans) : continue
				var takeable_piece : Spatial
				takeable_piece=get_node_in_cell(new_map_trans)
				if takeable_piece == null:
						continue
				if takeable_piece.player_number != piece.player_number:
					list_of_takable_pieces.append(takeable_piece)
			#  z in [-2;2] 
			for i in range(2):
				# x in [-1;1] 
				for j in range(2):
					new_map_trans = piece_map_trans + Vector3((j+1)*2-3, 0, 2 * ((i+1)*2-3) )
					if not cell_within_bounds(new_map_trans) : continue
					var takeable_piece : Spatial
					takeable_piece = get_node_in_cell(new_map_trans)
					if takeable_piece == null:
						continue
					if takeable_piece.player_number != piece.player_number:
						list_of_takable_pieces.append(takeable_piece)
					
		Globals.PIECE_TYPE.PAWN:
			for i in range(2):
				new_map_trans = piece_map_trans + Vector3(direction, 0, (i+1)*2-3)
				if not cell_within_bounds(new_map_trans) : continue
				var takeable_piece : Spatial
				takeable_piece = get_node_in_cell(new_map_trans)
				if takeable_piece == null:
					continue
				if takeable_piece.player_number != piece.player_number:
					list_of_takable_pieces.append(takeable_piece)

	return list_of_takable_pieces

func clear_piece_tray(player_index: int):
	for x in range(player_piece_trays[player_index].size()):
		for z in range(player_piece_trays[player_index][x].size()):
			player_piece_trays[player_index][x][z] = null

func clear_board():
	for x in range(inhabitants.size()):
		for z in range(inhabitants[x].size()):
			empty_cell(Vector3(x, 0, z))

func clear_everything():
	clear_piece_tray(0)
	clear_piece_tray(1)
	clear_board()
